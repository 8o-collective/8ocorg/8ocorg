FROM python:3.10-alpine
RUN apk add git
ARG BRANCH
EXPOSE 8000
ENV UVICORN_HOST 0.0.0.0
WORKDIR /app
COPY dispatch.py .
COPY modules ./modules
COPY scripts ./scripts
RUN python scripts/artifacts.py && python scripts/megalock.py
RUN pip install pipenv uvicorn
RUN pipenv install --system --ignore-pipfile --deploy
CMD ["uvicorn", "dispatch:app", "--proxy-headers"]