If you're reading this, a theoretical thermonuclear bomb has impacted our servers, and we need set everything up again.

Thankfully, we've had some foresight, and written this handy little guide to set up the services neccesary to reset everything anew. Read this guide very carefully - each sentence is vital.

First, set up the 8ocorg group in GitLab. Make sure that the group is set to public. Create the root repository, 8ocorg/8ocorg, the module subgroup, and the helpers and manifests projects.

Before you start pushing the modules back onto to what can only be assumed to be a smoldering pile of radioactive rubble, you will have to set up some group CI/CD variables in the 8ocorg group:

- DEVELOPMENT_HOST: this will be the domain of the server that is pushed to when the development branch is updated
- PRODUCTION_HOST: this will be the domain of the server that is pushed to when the production branch is updated

Finally:

- SSH_PRIVATE_KEY: (generate an ssh public/private key pair)[https://docs.gitlab.com/ee/user/ssh.html] and set this variable to the private key -- make sure this is set to protected

The public portion of this key has to be set as a (deploy key)[https://docs.gitlab.com/ee/user/project/deploy_keys/] for the root directory.

Make sure that the protected branches can also be pushed to by the new deploy key.

Before pushing any modules, make certain that the option that artifacts from the pipelines are kept indefinitely until a new one is pushed. If this is not checked, the builds for the cluster will fail. It should be checked by default but double-check just in case.

Make sure that all groups and repositories are public.

Now, we must discuss the cluster.

The cluster is created with GKE. It can be trivially connected to by selecting the "Connect" button in the dropdown and copying the resulting code. Install Helm on the client system.

[An agent must be installed on the cluster](https://docs.gitlab.com/ee/user/clusters/agent/install/) in order for the root runner to interface with it. This can be installed with Helm.

Create a static IP on the GKE cluster with ``gcloud compute addresses create web-static-ip --global``.

Now we can push the repositories.

Regarding the order in which the repositories must be published:
1. the root repository WITHOUT the .gitlab-ci.yml file.
2. the templates repository
3. the helpers repository
4. the chart repository
5. the modules (they should push themselves to the root repository)
6. the .gitlab-ci.yml file in the root directory

Set an A record for * for each of the domains to the IP that we generated earlier.