from io import BytesIO
import os
import urllib.request
import zipfile

appDirectory = os.path.join(os.path.realpath(os.path.dirname(__file__)), "../")
lockDirectory = os.path.join(appDirectory, "locks/")
moduleDirectory = os.path.join(appDirectory, "modules/")

artifactLink = lambda module: f"https://gitlab.com/8o-collective/8ocorg/modules/{module}/-/jobs/artifacts/{os.environ['BRANCH']}/download?job=build"

os.makedirs(lockDirectory, exist_ok=True)

for root, dirs, files in os.walk(moduleDirectory):
    if dirs or files:
        continue

    module = os.path.relpath(root, moduleDirectory)

    print(f"Downloading build artifacts for {module}...")

    with urllib.request.urlopen(artifactLink(module)) as compressed:
        with zipfile.ZipFile(BytesIO(compressed.read()), 'r') as archive:
            archive.extractall("/tmp/")
            os.rename("/tmp/dist", root)
    
    os.rename(os.path.join(root, "Pipfile.lock"), os.path.join(lockDirectory, f"{module.replace('/', '.')}.lock"))
