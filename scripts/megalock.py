import functools
import json
import os
import shutil

appDirectory = os.path.join(os.path.realpath(os.path.dirname(__file__)), "../")
lockDirectory = os.path.join(appDirectory, "locks/")

megalock = {
	"_meta": {
		"hash": {
			"sha256": "0" * 64
		},
		"pipfile-spec": 6,
		"requires": {
			"python_version": "3.10"
		},
		"sources": [
			{
				"name": "pypi",
				"url": "https://pypi.org/simple",
				"verify_ssl": True
			}
		]
	},
	"default": functools.reduce(
		lambda defaults, lockfile: defaults | json.load(open(os.path.join(lockDirectory, lockfile), 'r'))["default"], 
		os.listdir(lockDirectory),
		{},
	),
	"develop": {}
}

if __name__ == "__main__":
	with open(os.path.join(appDirectory, "Pipfile.lock"), "w+") as f:
		f.write(json.dumps(megalock, indent=4))
	
	with open(os.path.join(appDirectory, "Pipfile"), "w+") as f:
		f.write("""
		[[source]]
		url = "https://pypi.python.org/simple"
		verify_ssl = true
		name = "pypi"

		[packages]

		[dev-packages]
		""")
	
	shutil.rmtree(lockDirectory)