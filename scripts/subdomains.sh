#!/bin/sh

MODULES=$(find modules -type d -links 2)
SUBDOMAINS=""

for module in $MODULES
do
	module=${module#*modules} 
	module=${module%/main}

	IFS=/; set -f
	set -- $module$IFS # split string into "$@" array
	unset pass
	for i do # reverse "$@" array
	set -- "$i" ${pass+"$@"}
	pass=1
	done
	module=$(echo "$*") # "$*" to join the array elements with the first character of $IFS
	subdomain=${module//\//.}
	SUBDOMAINS="${SUBDOMAINS},${subdomain}"
done

echo \{${SUBDOMAINS:1}\}