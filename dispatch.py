import os
import importlib
from threading import Lock

hostname = os.environ['HOST'] # set by env in the deployment.yaml manifest in the chart
moduleDirectory = os.path.join(os.path.dirname(os.path.realpath(__file__)), "modules/")

async def NotFound(scope, recieve, send):
    """
    Module not found, return 404 and send HTML that redirects to invite.8oc.org
    """
    data = f'<!doctype html><html><head><meta http-equiv="refresh" content="0; URL=http://invite.{hostname}" /></head></html>'.encode('utf-8')
    await send({
        'type': 'http.response.start',
        'status': 404,
        'headers': [
            [b'content-type', b'text/html; charset=UTF-8'],
            [b'content-length', str(len(data)).encode('utf-8')]
        ]
    })
    return await send({
        'type': 'http.response.body',
        'body': data,
    })

class SubdomainDispatcher(object):
    def __init__(self, domain):
        self.domain = domain
        self.lock = Lock()
        self.instances = {}

    def create_app(self, subdomain):
        if subdomain: 
            subdomainRelativePath = '/'.join(subdomain.split('.')[::-1]) # Split dots, reverse, and add slashes
            subdomainPath = os.path.join(moduleDirectory, subdomainRelativePath)

            if os.path.isdir(subdomainPath):
                if not os.path.exists(os.path.join(subdomainPath, "server/")):
                    if os.path.exists(os.path.join(subdomainPath, "main/")):
                        subdomain = "main." + subdomain # Go to parentmodule/main if we just go to parentmodule.8oc.org
                    else:
                        return NotFound
            else:
                return NotFound
        else:
            subdomain = "main"
            
        server = importlib.import_module(f"modules.{'.'.join(subdomain.split('.')[::-1])}.server")
        return server.app

    def get_application(self, host):
        subdomain = host[:-len(self.domain)].rstrip('.')
        with self.lock:
            app = self.instances.get(subdomain)
            if app is None:
                app = self.create_app(subdomain)
                self.instances[subdomain] = app
            return app

    async def __call__(self, scope, recieve, send):
        host = dict(scope['headers'])[b'host'].decode('utf-8').split(':')[0]
        if not host.endswith(self.domain):
            await send({'type': 'http.response.start', 'status': 400})
            return await send({'type': 'http.response.body', 'body': b'Invalid header',})
        app = self.get_application(host)
        return await app(scope, recieve, send)

app = SubdomainDispatcher(hostname.split(':')[0])