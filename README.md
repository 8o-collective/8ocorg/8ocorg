# *.8oc.org

The 8oc.org production root directory.

## NOTE: ALL OF THE FOLLOWING IS OUTDATED AND NEEDS TO BE REWRITTEN.

## I want to develop. Where do I start?

Firstly, thank you for wanting to develop. you fuck.

This repository isn't designed for development, so you don't need to clone it for development. 

Clone the module directory of the module that you want to work on. For example, if you wanted to work on ``musings.redlog.8oc.org``, you would clone [``redlog/musings``](https://gitlab.com/8o-collective/8ocorg/redlog/musings). Then, you can commit changes and push straight to ``development``, or if your implementation will take a while to develop, checkout a new feature branch and merge it into ``development`` later.

You will need 3 main tools to develop on a module: [yarn classic](https://classic.yarnpkg.com/lang/en/), [pipenv](https://pipenv.pypa.io/en/latest/), and [make](https://www.gnu.org/software/make/).

For most development purposes, simply run ``make`` in the module directory after cloning. This will install all yarn packages and all pipenv packages when first run, and will subsequently start the backend and frontend.

Most modules have a frontend, but some modules are API-only (like [``wizx``](https://gitlab.com/8o-collective/8ocorg/wizx/)). These modules do not have a ``yarn start`` and their ``yarn build`` only returns ``Nothing to build.``

Both of the dev servers restart on local file changes and do not to be manually restarted when a file is changed.

When you are finished developing, please check your code with ``make lint`` and ``make format``. Note that the latter of these will modify code in-place with no confirmation, so make a backup if there's anything really important formatting-wise that you think the algorithm may mess with.

## I want to push to production. What should I do?

Well well well, you think your changes are good enough to push to production. Tsk.

Unless you're making an entirely new module (discussed below), you don't need to clone this repository to push to production.

After committing and pushing to the ``development`` branch of the module of your choice, make a commit with message ``vX.X.X`` and ONLY change the version number in ``package.json`` to this new version. Then, merge ``development`` into ``production``.

NOTE: Version numbers go [major].[minor].[patch]

To see any production changes on [8oc.org](http://8oc.org), the Docker image on Federado will have to be rebuilt. This can be done by SSHing into Federado, running ``git submodule update --init --recursive`` and ``docker-compose down && docker-compose up -d --build``.

## I want to make a new module or add an existing one to production. How do I do that?

To make a new module, start by making a new module repository. There are a few rules to note here when creating a new module repository:

- A module repository can either be in the main project ([``8ocorg``](https://gitlab.com/8o-collective/8ocorg/)), or in a subproject (like [``redlog``](https://gitlab.com/8o-collective/8ocorg/redlog/)). If the module is in a subproject, it is considered a submodule of the parent module above it. Submodules look the same as top-level modules, but parent modules follow special rules:
	- Some parent modules may have a ``main`` repository (like [``redlog/main``](https://gitlab.com/8o-collective/8ocorg/redlog/main)) which defines what happens when the module is accessed with no submodule specified. ([``redlog/main``](https://gitlab.com/8o-collective/8ocorg/redlog/main) defines ``redlog.8oc.org``)

After the module is created, feel free to copy the code from an existing module to get started. It is highly reccommended to use [``invite``](https://gitlab.com/8o-collective/8ocorg/invite/) as that module has no extra dependencies and is generally a good example of how modules should be structured.

NOTE: Make absolutely *certain* that you have changed the name field in ``package.json`` to the name of the new module before pushing. If you do not do this, the module will break in production. Also make sure that you've changed the name and TODOs in ``README.md``.

When a new module is created in any project, it must be specified in the parent module identifier in order to be pushed to production. This can be done by cloning the identifier, running ``git submodule add https://link/to/module``, making a commit message ``Added submodule [name of the module]`` and pushing.

NOTE: For the ``8ocorg`` idenfier, the command to add a submodule must be ``git submodule add https://link/to/module ./modules/[submodule]`` before pushing.

Like any other production change, the Docker image on Federado will have to be rebuilt before changes are seen on [8oc.org](http://8oc.org). This process is described in the above section.